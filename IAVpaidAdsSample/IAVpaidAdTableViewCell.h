//
//  IAVpaidAdTableViewCell.h
//  IAVpaidAdsSample
//
//  Created by Avi Gelkop on 1/17/18.
//  Copyright © 2018 Inneractive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IAVpaidAdTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *vpaidAdUrl;
@property (weak, nonatomic) IBOutlet UIImageView *indicatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *parametersAvailableLabel;
@property (weak, nonatomic) IBOutlet UIImageView *playingSuccessImage;

@end
