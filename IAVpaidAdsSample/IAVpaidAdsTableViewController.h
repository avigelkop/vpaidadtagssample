//
//  IAVpaidAdsTableViewController.h
//  IAVpaidAdsSample
//
//  Created by Avi Gelkop on 1/17/18.
//  Copyright © 2018 Inneractive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAVpaidAdTableViewCell.h"
#import "SettingsViewController.h"


@interface IAVpaidAdsTableViewController : UITableViewController {
    NSMutableArray *ads;
}

@end
