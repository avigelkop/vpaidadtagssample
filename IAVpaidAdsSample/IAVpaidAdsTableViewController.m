//
//  IAVpaidAdsTableViewController.m
//  IAVpaidAdsSample
//
//  Created by Avi Gelkop on 1/17/18.
//  Copyright © 2018 Inneractive. All rights reserved.
//

#import "IAVpaidAdsTableViewController.h"
#import "SharedData.h"
#import "VpaidPlayerViewController.h"


@interface IAVpaidAdsTableViewController ()

@property (nonatomic, strong) SettingsViewController *settingsViewController;
@property (nonatomic, strong) UIBarButtonItem *settingsButton;

@end

@implementation IAVpaidAdsTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self addSettingsButton];
    [self setClientRequestSettings];
    [self fetch];
    
}
//Adding settings buttom
- (void)addSettingsButton {
    _settingsButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Settings", @"Settings button title") style:UIBarButtonItemStylePlain target:self action:@selector(settingsButtonWasPressed:)];
    NSDictionary *titleAttributes = [NSDictionary dictionaryWithObject:[UIColor orangeColor] forKey:NSForegroundColorAttributeName];
    [self.settingsButton setTitleTextAttributes:titleAttributes forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = self.settingsButton;
}

//Click on settings button
- (void)settingsButtonWasPressed:(UIButton *)button {
    if (nil == self.settingsViewController) {
        self.settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    }
    //Adding new navigation controller
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.settingsViewController];
    navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    navigationController.navigationBar.translucent = NO;
    
    //Adding done button to the settings view controller navigator
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonWasPressed:)];
    NSDictionary* titleAttributes = [NSDictionary dictionaryWithObject:[UIColor orangeColor] forKey:NSForegroundColorAttributeName];
    [doneItem setTitleTextAttributes:titleAttributes forState:UIControlStateNormal];
    self.settingsViewController.navigationItem.rightBarButtonItem = doneItem;
    [self presentViewController:navigationController animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//After done was pressed we are doing 3 things:
//1. Setting the request settings.
//2. Calling for method for checking the ads tags values
//3. Reloading the data

- (void)doneButtonWasPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        [weakSelf setClientRequestSettings];
        [weakSelf getAdsValues];
        [self fetch];
        [self.tableView reloadData];
    }];
    
}

- (void)setClientRequestSettings {
    [SharedData sharedInstance].spotID = self.settingsViewController.selectedSpotTextView.text;
    [SharedData sharedInstance].adTags = self.settingsViewController.adTags;
    [SharedData sharedInstance].VPAIDPlayerURLString = self.settingsViewController.jsTagUrlTextView.text;
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ads count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IAVpaidAdTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VpaidAdCell" forIndexPath:indexPath];
    [cell prepareForReuse];
    cell.vpaidAdUrl.text = [NSString stringWithFormat:@"%ld) %@", (long)indexPath.row , ads[indexPath.row][0]];
    
    if ([ads[indexPath.row] count] > 1){
        cell.parametersAvailableLabel.text = @"Parameters available";
        cell.parametersAvailableLabel.textColor = [UIColor colorWithRed:(55/255.0) green:(200/255.0) blue:(55/255.0) alpha:1.0];
    }
    else{
        cell.parametersAvailableLabel.text = @"Parameters not available";
        cell.parametersAvailableLabel.textColor = [UIColor grayColor];

    }
    
    if([[SharedData sharedInstance].playingSuccess[indexPath.row] isEqual: @"1"]){
        cell.playingSuccessImage.image = [UIImage imageNamed:@"success.png"];
    }
    else if ([[SharedData sharedInstance].playingSuccess[indexPath.row] isEqual: @"-1"]){
        cell.playingSuccessImage.image = [UIImage imageNamed:@"failure.png"];
    }
    else{
        cell.playingSuccessImage.image = NULL;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VpaidPlayerViewController *vpaidPlayer = [self.storyboard instantiateViewControllerWithIdentifier:@"vpaidPlayer"];
    vpaidPlayer.adIndex = indexPath;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vpaidPlayer];
    navigationController.navigationBar.barTintColor = [UIColor colorWithRed:(201/255.0) green:(255/255.0) blue:(254/255.0) alpha:1.0];
    navigationController.navigationBar.translucent = NO;
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"") style:UIBarButtonItemStylePlain target:self action:@selector(backButtonWasPressed:)];
    NSDictionary* titleAttributes = [NSDictionary dictionaryWithObject:[UIColor orangeColor] forKey:NSForegroundColorAttributeName];
    [back setTitleTextAttributes:titleAttributes forState:UIControlStateNormal];
    vpaidPlayer.navigationItem.rightBarButtonItem = back;
    
    [self.navigationController pushViewController:vpaidPlayer animated:YES];
}

- (void)backButtonWasPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self.tableView reloadData];

    
}

- (void) getAdsValues {
    if([SharedData sharedInstance].getAdTags)
        ads = [SharedData sharedInstance].getAdTags;
}

-(void) fetch{
    ads = [[SharedData sharedInstance] updateCSVFile];
}


@end
