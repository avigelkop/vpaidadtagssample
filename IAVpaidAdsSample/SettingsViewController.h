//
//  SettingsViewController.h
//  IAVpaidAdsSample
//
//  Created by Avi Gelkop on 1/17/18.
//  Copyright © 2018 Inneractive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController <UIPickerViewDelegate , UIPickerViewDataSource>{
    NSArray *units;
    NSArray *spots;
}

@property (weak, nonatomic) IBOutlet UIPickerView *unitPickerView;
@property (weak, nonatomic) IBOutlet UIButton *unitChoosingButton;
@property (weak, nonatomic) IBOutlet UITextField *csvFilePathTextView;
@property (weak, nonatomic) IBOutlet UILabel *selectedUnitLabel;
@property (weak, nonatomic) IBOutlet UITextField *selectedSpotTextView;
@property (weak, nonatomic) IBOutlet UITextField *jsTagUrlTextView;
@property (nonatomic) NSMutableArray *adTags;

@end
