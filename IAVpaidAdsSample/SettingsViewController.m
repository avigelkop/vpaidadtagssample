//
//  SettingsViewController.m
//  IAVpaidAdsSample
//
//  Created by Avi Gelkop on 1/17/18.
//  Copyright © 2018 Inneractive. All rights reserved.
//

#import "SettingsViewController.h"
#import "SharedData.h"


@interface SettingsViewController () <UITextFieldDelegate>

@end

@implementation SettingsViewController


//NSString * const _Nonnull kIAVPAIDPlayerURLString = @"https://cdn2.inner-active.mobi/client/ia-js-tags/ia-tag-sdk.min-ios-7.0.6.js";


- (void)viewDidLoad {
    //some settings for the keyboard:
    self.jsTagUrlTextView.delegate = self;
    self.selectedSpotTextView.delegate = self;
    self.csvFilePathTextView.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    self.navigationItem.title = @"Settings";
    spots = @[@"150945",@"150946",@"150947",@"150948",@"150949"];
    units = @[@"mrect video",@"interstitial",@"landscape",@"square",@"rewarded"];

    [super viewDidLoad];
    self.unitPickerView.delegate = self;
    self.unitPickerView.dataSource = self;
    self.unitPickerView.hidden = true;
    self.jsTagUrlTextView.text = @"https://cdn2.inner-active.mobi/client/ia-js-tags/ia-tag-sdk.min-ios-7.0.6.js";
    self.adTags = [SharedData sharedInstance].adTags;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)fetchCSVFile:(id)sender {
    NSLog(@"fetching csv file");
    //dedault csv:
    if ([self.csvFilePathTextView.text isEqual: @""]){
        [SharedData sharedInstance].filePath = [[NSBundle mainBundle] pathForResource:@"vpaidTestingDemo" ofType:@"csv"];
        self.adTags = [[SharedData sharedInstance] updateCSVFile];
        
    }
    //download from source
    else{
        NSURL  *url = [NSURL URLWithString:self.csvFilePathTextView.text];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"filename.png"];
            [urlData writeToFile:filePath atomically:YES];
            //update the shared data
            [SharedData sharedInstance].filePath = filePath;
            [[SharedData sharedInstance] updateCSVFile];
        }
        
    }
}

- (IBAction)chooseUnit:(id)sender {
    self.unitPickerView.hidden = false;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.selectedUnitLabel.text = [units objectAtIndex:row];
    self.selectedSpotTextView.text = [spots objectAtIndex:row];
    self.unitPickerView.hidden = true;
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return units.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return units[row];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
