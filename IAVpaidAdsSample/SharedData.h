//
//  ClientRequestSettings.h
//  IASDKClient
//
//  Created by Inneractive on 17/04/2017.
//  Copyright © 2017 Inneractive. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <IASDKCore/IAInterfaceSingleton.h>

@class IAAdRequest;

@interface SharedData : NSObject <IAInterfaceSingleton>

+ (instancetype _Nonnull)sharedInstance;

@property (nonatomic, strong, nullable) NSString *spotID;
@property (nonatomic, strong, nullable) NSString *VPAIDPlayerURLString;
@property (nonatomic, strong, nullable) NSMutableArray *adTags;
@property (nonatomic, strong, nullable) NSMutableArray *playingSuccess;
@property (nonatomic, strong, nullable) NSString *filePath;




/**
 *  @brief Updates request settings according to client app settings fields.
 *  @discussion Should be invoked after request creation and before passing the request object to any other object.
 */
- (void)updateRequestObjectWithCustomSettings:(IAAdRequest * _Nonnull)request;
- (NSMutableArray * _Nullable)updateCSVFile;
- (NSMutableArray * _Nullable)getAdTags;



@end
