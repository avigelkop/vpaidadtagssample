//
//  ClientRequestSettings.m
//  IASDKClient
//
//  Created by Inneractive on 17/04/2017.
//  Copyright © 2017 Inneractive. All rights reserved.
//

#import "SharedData.h"

#import <IASDKCore/IASDKCore.h>
#import <IASDKCore/IADebugger.h>

@interface SharedData ()

#pragma mark - Public redefinition

@property (nonatomic, strong, nullable, readwrite) NSString *appIDFieldInCustomSettings;

@end

@implementation SharedData {}

#pragma mark - Static Inits

+ (void)load {
    [self.class sharedInstance];
}

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    if (!_sharedInstance) {
        dispatch_once(&onceToken, ^{
            _sharedInstance = [[self.class allocWithZone:nil] init];
			
            [[NSNotificationCenter defaultCenter] addObserver:_sharedInstance selector:@selector(updateVPAIDPlayerURLString:) name:kIADebuggerDidChangeRequestSettingsNotification object:nil];
        
        });
    }
    
    return _sharedInstance;
}

#pragma mark - Notifications

- (void)updateVPAIDPlayerURLString:(NSNotification *)notification {
	if ([notification.object isKindOfClass:[IADebugger class]]) { 
		IADebugger *debugger = (IADebugger *)notification.object;
		self.VPAIDPlayerURLString = debugger.VPAIDPlayerURLString;
	}
}

#pragma mark - API


- (void)updateRequestObjectWithCustomSettings:(IAAdRequest * _Nonnull)request {
    
    // updating only if has non-empty object; if certain field is nil, means this field was never changed;
    if (self.spotID) {
        request.spotID = self.spotID;
    }
    if (self.VPAIDPlayerURLString) {
        request.debugger.VPAIDPlayerURLString = self.VPAIDPlayerURLString;
    }
}

- (NSMutableArray * _Nullable)getAdTags{
    //should return array: [path,parameters]
    return self.adTags;
}


- (NSMutableArray * _Nullable)updateCSVFile{
    NSLog(@"updating the csv file");
    //if file path is empty take the defualt
    if(!self.filePath){
        self.filePath = [[NSBundle mainBundle] pathForResource:@"vpaidTestingDemo" ofType:@"csv"];
    }
    //read the file
    self.adTags = [NSMutableArray array];
    NSString* fileContents = [NSString stringWithContentsOfFile:self.filePath encoding:NSUTF8StringEncoding error:nil];
    //seperate the file into lines
    NSArray* rows = [fileContents componentsSeparatedByString:@"\n"];
    for (NSString *row in rows){
        NSMutableArray *tagAndParameterArray = [NSMutableArray array];
        NSArray* columns = [row componentsSeparatedByString:@"\",\""];
        NSString *tag = columns[0];
        tag = [tag stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        [tagAndParameterArray addObject:tag];
        if([columns count] > 1){
            NSString *parameter = columns[1];
            parameter = [parameter stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            [tagAndParameterArray addObject:parameter];
            [self.adTags addObject:tagAndParameterArray];
        }
        else{
            [self.adTags addObject:tagAndParameterArray];
        }
    }
    self.adTags = self.adTags;
    return self.adTags;
}


@end
