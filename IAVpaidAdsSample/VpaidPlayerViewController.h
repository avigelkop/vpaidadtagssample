//
//  VpaidPlayerViewController.h
//  IAVpaidAdsSample
//
//  Created by Avi Gelkop on 1/17/18.
//  Copyright © 2018 Inneractive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@class IAAdSpot;
@class IAAdModel;

@interface VpaidPlayerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic) NSIndexPath *adIndex;


@end
