//
//  VpaidPlayerViewController.m
//  IAVpaidAdsSample
//
//  Created by Avi Gelkop on 1/17/18.
//  Copyright © 2018 Inneractive. All rights reserved.
//

#import "VpaidPlayerViewController.h"

#import <IASDKCore/IASDKCore.h>
#import <IASDKVideo/IASDKVideo.h>

#import "SharedData.h"


@interface VpaidPlayerViewController () <IAUnitDelegate,IAVideoContentDelegate>

@property (nonatomic, strong) IAViewUnitController *viewUnitController;
@property (nonatomic, strong) IAVideoContentController *videoContentController;
@property (nonatomic, strong, nonnull) IAFullscreenUnitController *fullscreenUnitController;

@property (nonatomic, strong) IAAdSpot *adSpot;
@property (nonatomic, weak) IAAdView *adView;

@property (nonatomic, strong) NSMutableArray *adTags;
@end

@implementation VpaidPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    if (![SharedData sharedInstance].playingSuccess){
        [SharedData sharedInstance].playingSuccess = [[NSMutableArray arrayWithCapacity:[[SharedData sharedInstance].adTags count]] init];
        for (int i =0 ; i< [[SharedData sharedInstance].adTags count]; i++){
            [[SharedData sharedInstance].playingSuccess addObject:[NSNull null]];
        }
    }
    self.spinner.transform = CGAffineTransformMakeScale(2.25, 2.25);
    self.spinner.hidesWhenStopped =YES;
    [self.spinner startAnimating];
    [self loadNewAdOfType];
    
    
    
    
}


- (void)loadNewAdOfType{
    self.adTags = [[SharedData sharedInstance] getAdTags];    
    IAAdRequest *request =
    [IAAdRequest build:^(id<IAAdRequestBuilder> _Nonnull builder) {
        
        builder.useSecureConnections = NO;
        builder.spotID = @"150945";
        builder.timeout = 10;
        builder.debugger = [IADebugger build:^(id<IADebuggerBuilder>  _Nonnull builder) {
            builder.database = @"4002";
            builder.server = @"ia-test";
            builder.mockResponsePath= @"7714";
            builder.jsTagResponse = [self getVpaidResponse];
        }];
        
    }];
    [[SharedData sharedInstance] updateRequestObjectWithCustomSettings:request];
    
    
    _videoContentController = [IAVideoContentController build:^(id<IAVideoContentControllerBuilder>  _Nonnull builder) {
        builder.videoContentDelegate = self;
    }];
    
    
    _viewUnitController = [IAViewUnitController build:^(id<IAViewUnitControllerBuilder>  _Nonnull builder) {
        builder.unitDelegate = self;
        
        [builder addSupportedContentController:self.videoContentController];
    }];
    
    
    _fullscreenUnitController = [IAFullscreenUnitController build:^(id<IAFullscreenUnitControllerBuilder>  _Nonnull builder) {
        builder.unitDelegate = self;
        
        [builder addSupportedContentController:self.videoContentController];
    }];
    
    
    _adSpot = [IAAdSpot build:^(id<IAAdSpotBuilder>  _Nonnull builder) {
        builder.adRequest = request;
        [builder addSupportedUnitController:self.viewUnitController]; // 'self' can be used in builder block, this block is not retained; the concept is similar to iOS method 'enumerateObjectsUsingBlock:';
        [builder addSupportedUnitController:self.fullscreenUnitController];
    }];
    
    __weak typeof(self) weakSelf = self;
    [self.adSpot fetchAdWithCompletion:^(IAAdSpot * _Nullable adSpot,IAAdModel * _Nullable adModel,NSError * _Nullable error) {
        [weakSelf renderAdWithSpot:adSpot model:adModel error:error];
    }];
}


- (void)renderAdWithSpot:(IAAdSpot * _Nullable)adSpot model:(IAAdModel * _Nullable)model error:(NSError * _Nullable)error {
    [self.spinner stopAnimating];
    if (error) {
        NSLog(@"Failed to get an ad: %@\n", error);
        [self.spinner stopAnimating];

        [SharedData sharedInstance].playingSuccess[self.adIndex.row] = @"-1";
    } else {
        if (adSpot.activeUnitController == self.viewUnitController) {
            [self.viewUnitController showAdInParentView:self.view];
            self.adView = self.viewUnitController.adView;
            
            if (self.adView) {
                self.adView.backgroundColor = [UIColor whiteColor];
                self.adView.translatesAutoresizingMaskIntoConstraints = NO;
                
                // adding centerX constraint
                [self.view addConstraint:
                 [NSLayoutConstraint constraintWithItem:self.adView
                                              attribute:NSLayoutAttributeCenterX
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:self.view
                                              attribute:NSLayoutAttributeCenterX
                                             multiplier:1
                                               constant:0]];
                
                // adding top constraint
                [self.view addConstraint:
                 [NSLayoutConstraint constraintWithItem:self.adView
                                              attribute:NSLayoutAttributeTop
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:self.view
                                              attribute:NSLayoutAttributeTop
                                             multiplier:1
                                               constant:70]];
            }
            
        }
        else if (adSpot.activeUnitController == self.fullscreenUnitController) {
            [self.fullscreenUnitController showAdAnimated:YES completion:nil];
        }
        
        if ([adSpot.activeUnitController.activeContentController isKindOfClass:IAVideoContentController.class]) { // 'is video ad' check;
            IAVideoContentController *videoContentController = (IAVideoContentController *)adSpot.activeUnitController.activeContentController;
    
            videoContentController.videoContentDelegate = self;
            videoContentController.muted = YES;
        }

    }
}


- (NSString *) getVpaidResponse{
    NSString *tag = self.adTags[self.adIndex.row][0];
    NSString *encodeTag= [tag stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    encodeTag= [encodeTag stringByReplacingOccurrencesOfString:@"\r" withString:@""];


    NSString *pathPre = [[NSBundle mainBundle] pathForResource:@"response"
                                                        ofType:@"txt"];

    NSString* prefix = [NSString stringWithContentsOfFile:pathPre
                                                  encoding:NSASCIIStringEncoding
                                                     error:NULL];
    NSString* ans = [prefix stringByReplacingOccurrencesOfString:@"xxxxxxxx" withString:encodeTag];
    return ans;
}


#pragma mark - InneractiveAdDelegate

- (UIViewController *)viewControllerForPresentingModalView{
    return self;
}


#pragma mark - IAUnitDelegate

- (UIViewController * _Nonnull)IAParentViewControllerForUnitController:(IAUnitController * _Nullable)unitController {
    return self;
}

- (void)IAAdDidReceiveClick:(IAUnitController * _Nullable)unitController {
    NSLog(@"ad clicked;");
}

- (void)IAAdWillLogImpression:(IAUnitController * _Nullable)unitController {
    NSLog(@"ad ipmression;");
    
}

- (void)IAUnitControllerWillPresentFullscreen:(IAUnitController * _Nullable)unitController {
    NSLog(@"IAUnitControllerWillPresentFullscreen");
}

- (void)IAUnitControllerDidPresentFullscreen:(IAUnitController * _Nullable)unitController {
    NSLog(@"IAUnitControllerDidPresentFullscreen");
}

- (void)IAUnitControllerWillDismissFullscreen:(IAUnitController * _Nullable)unitController {
    NSLog(@"IAUnitControllerWillDismissFullscreen");
}

// TODO: ensure in interstitial is not being called several times, e.g: in-app browser dismissed -> interstitial itself dismissed;
- (void)IAUnitControllerDidDismissFullscreen:(IAUnitController * _Nullable)unitController {
    NSLog(@"IAUnitControllerDidDismissFullscreen");
}

- (void)IAUnitControllerWillOpenExternalApp:(IAUnitController * _Nullable)unitController {
    NSLog(@"IA will open external app;");
}

#pragma mark - IAVideoContentDelegate

- (void)IAVideoCompleted:(IAVideoContentController * _Nullable)contentController {
    NSLog(@"IAVideoCompleted");
    [SharedData sharedInstance].playingSuccess[self.adIndex.row] = @"1";
    
}

- (void)IAVideoContentController:(IAVideoContentController * _Nullable)contentController videoInterruptedWithError:(NSError *)error {
    NSLog(@"videoInterruptedWithError");
    [SharedData sharedInstance].playingSuccess[self.adIndex.row] = @"-1";
    
}

- (void)IAVideoContentController:(IAVideoContentController * _Nullable)contentController videoDurationUpdated:(NSTimeInterval)videoDuration {
    NSLog(@"videoDurationUpdated");
}

- (void)IAVideoContentController:(IAVideoContentController * _Nullable)contentController videoProgressUpdatedWithCurrentTime:(NSTimeInterval)currentTime totalTime:(NSTimeInterval)totalTime {
    NSLog(@"videoProgressUpdatedWithCurrentTime: %.2f totalTime: %.2f", currentTime, totalTime);
}

@end
